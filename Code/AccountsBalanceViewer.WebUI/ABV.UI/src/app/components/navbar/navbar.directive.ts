import { AuthService } from '../../services/auth.service';

/** @ngInject */
export function acmeNavbar(): angular.IDirective {

  return {
    restrict: 'E',
    scope: {
      creationDate: '='
    },
    templateUrl: 'app/components/navbar/navbar.html',
    controller: NavbarController,
    controllerAs: 'vm',
    bindToController: true
  };

}

/** @ngInject */
export class NavbarController {
  public relativeDate: string;
  // "this.creationDate" is initialized by directive option "bindToController: true"
  public creationDate: number;

  constructor(moment: moment.MomentStatic, private authService: AuthService, private $location: ng.ILocationService) {
    this.relativeDate = moment(this.creationDate).fromNow();
  }

  public logOff() {
    this.authService.logoff();
    this.$location.path('/login');
  }

}
