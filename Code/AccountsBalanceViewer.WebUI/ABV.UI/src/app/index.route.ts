import { AuthService } from './services/auth.service.ts'

/** @ngInject */
export class routerConfig {

  constructor(private $stateProvider: angular.ui.IStateProvider,
    private $urlRouterProvider: angular.ui.IUrlRouterProvider) {
    this.init();
  }

  public init() {
    this.$stateProvider
    .state('dashboard', {
      url: '/',
      templateUrl: 'app/modules/dashboard/dashboard.html',
      controller: 'DashboardController',
      controllerAs: 'dashboardCtrl',
        resolve: {
          authorize: ["authService", "$q", (authService, $q) => { return this.ResolveAuth(authService, $q) }]
        }
    })
      .state('fileupload', {
      url: '/fileupload',
      templateUrl: 'app/modules/fileupload/fileupload.html',
      controller: 'FileUploadController',
      controllerAs: 'fileUploadCtrl',
        resolve: {
          authorize: ["authService", "$q", (authService, $q) => { return this.ResolveAuth(authService, $q) }]
        }
    })
      .state('report', {
      url: '/report',
      templateUrl: 'app/modules/report/report.html',
      controller: 'ReportController',
      controllerAs: 'reportCtrl',
        resolve: {
          authorize: ["authService", "$q", (authService, $q) => { return this.ResolveAuth(authService, $q) }]
        }
    })
    .state('login', {
        url: '/login',
        templateUrl: 'app/modules/login/login.html',
        controller: 'LoginController',
        controllerAs: 'loginCtrl'
      });


    this.$urlRouterProvider.otherwise('/');
  }

  private ResolveAuth(authService, $q) {
    var deferred = $q.defer();
    if (authService.isAuthorize()) {
      deferred.resolve();
    } else {
      var error = { code: 404 };
 deferred.reject(error);
    }
    return deferred.promise;
  }
}


// /** @ngInject */
// export function routerConfig($stateProvider: angular.ui.IStateProvider, $urlRouterProvider: angular.ui.IUrlRouterProvider) {
//   $stateProvider
//     .state('dashboard', {
//       url: '/',
//       templateUrl: 'app/modules/dashboard/dashboard.html',
//       controller: 'DashboardController',
//       controllerAs: 'dashboardCtrl',
//         resolve: {
//           authorize: ["authService", "$q", (authService, $q) => { return this.ResolveAuth(authService, $q) }]
//         }
//     })
//       .state('fileupload', {
//       url: '/fileupload',
//       templateUrl: 'app/modules/fileupload/fileupload.html',
//       controller: 'FileUploadController',
//       controllerAs: 'fileUploadCtrl'
//     })
//       .state('report', {
//       url: '/report',
//       templateUrl: 'app/modules/report/report.html',
//       controller: 'ReportController',
//       controllerAs: 'reportCtrl'
//     })
//     .state('login', {
//         url: '/login',
//         templateUrl: 'app/modules/account/login.html',
//         controller: 'LoginController',
//         controllerAs: 'loginCtrl'
//       });

//   $urlRouterProvider.otherwise('/');

// }

