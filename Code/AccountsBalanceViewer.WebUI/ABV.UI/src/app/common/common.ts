export interface ICommonConst {
    API_HOST: string;
    AUTH_TOKEN_KEY: string;
    USER_ID: string;
    ROLE: string;
}

export function GetCommonConst(): ICommonConst {
    return {
        // API_HOST: 'http://localhost:9000/',
        API_HOST: 'http://accountbalanceviewerapi.azurewebsites.net/',
        AUTH_TOKEN_KEY: 'auth_token',
        USER_ID: 'user_id',
        ROLE: 'role',
    };
}