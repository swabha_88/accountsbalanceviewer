/// <reference path="../../typings/main.d.ts" />

import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { GithubContributor } from '../app/components/githubContributor/githubContributor.service';
import { WebDevTecService } from '../app/components/webDevTec/webDevTec.service';
import { acmeNavbar } from '../app/components/navbar/navbar.directive';
import { acmeMalarkey } from '../app/components/malarkey/malarkey.directive';

//common
import { GetCommonConst, ICommonConst } from '../app/common/common';

//services
import { FileUploadService } from '../app/services/fileupload.service';
import { LookupService } from '../app/services/lookup.service';
import { DashboardService } from '../app/services/dashboard.service';
import { AuthService } from '../app/services/auth.service';
import { AuthInterceptorService } from '../app/services/authInterceptor.service';

//controllers
import { DashboardController } from './modules/dashboard/dashboard.controller';
import { FileUploadController } from './modules/fileupload/fileupload.controller';
import { ReportController } from './modules/report/report.controller';
import { LoginController } from './modules/login/login.controller';

declare var malarkey: any;
declare var moment: moment.MomentStatic;

module abvUi {
  'use strict';

  angular.module('abvUi', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'ui.bootstrap', 'toastr',
   'angularFileUpload','highcharts-ng','LocalStorageModule'])
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .config(config)
   .config(["$stateProvider", "$urlRouterProvider", ($stateProvider, $urlRouterProvider) => {
            return new routerConfig($stateProvider, $urlRouterProvider);
        }])
        .constant('commonConst', GetCommonConst())
 .run(runBlock)
           .service('authService', AuthService)
 .service('githubContributor', GithubContributor)
    .service('webDevTec', WebDevTecService)
       .service('fileUploadService', FileUploadService)
       .service('lookupService', LookupService)
       .service('dashboardService', DashboardService)
                .service('authInterceptorService', AuthInterceptorService)
   .controller('LoginController', LoginController)
 .controller('FileUploadController', FileUploadController)
    .controller('ReportController', ReportController)
 .controller('DashboardController', DashboardController)
    .directive('acmeNavbar', acmeNavbar)
    .directive('acmeMalarkey', acmeMalarkey);
}
