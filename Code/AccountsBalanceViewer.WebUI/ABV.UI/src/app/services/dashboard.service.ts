import { ICommonConst } from '../common/common';

export interface IAccounts {
    Id;
    Year;
    Month;
    Price;
}

export class DashboardService {

    /* @ngInject */
    constructor(
        private $http: ng.IHttpService,
        private commonConst: ICommonConst) {
    }

    getExpenses(year,month): ng.IPromise<IAccounts[]> {
        return this.$http.get(this.commonConst.API_HOST + 'api/Expenses?year='+year+'&month='+month).then(response => {
            console.log(response)
            return response.data;
        });
    }
}