import { ICommonConst } from '../common/common'

export interface ILookupConstant {
    Years;
    Months;
}


export class LookupService {

    /* @ngInject */
    constructor(
        private $http: ng.IHttpService,
        private $q: ng.IQService,
        private commonConst: ICommonConst,
        private localStorageService: angular.local.storage.ILocalStorageService) {
    }

    getConstants(): ng.IPromise<ILookupConstant[]> {
        var deferred = this.$q.defer();
            this.$http.get(this.commonConst.API_HOST + 'api/lookup/GetConstants').then(response => {
                deferred.resolve(<ILookupConstant[]>response.data);
            }).catch(reason => {
                deferred.reject(reason);
            });
             return deferred.promise;
    }

}