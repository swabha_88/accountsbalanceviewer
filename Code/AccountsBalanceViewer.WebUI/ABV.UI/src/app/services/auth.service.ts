import { ICommonConst } from '../common';

export class AuthService {

    /* @ngInject */
    constructor(
        private $http: ng.IHttpService,
        private commonConst: ICommonConst,
        private localStorageService: angular.local.storage.ILocalStorageService) {
    }

    public login(userName: string, password: string): ng.IPromise<string> {
        var authDataString = "grant_type=password&username=" + userName + "&password=" + password;

        // var header = new Headers({
        // 'Content-Type': 'application/x-www-form-urlencoded'
        // });

        // let body = new URLSearchParams();
        // body.set('username', userName);
        // body.set('password', password);

        return this.$http.post(this.commonConst.API_HOST + 'token', authDataString, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).then(response => {
            var data = <any>response.data;
          
            if (!(data.access_token == null || data.access_token == undefined)) {
                this.localStorageService.set(this.commonConst.AUTH_TOKEN_KEY, data.access_token);
            }
        
            if (!(data.id == null || data.id == undefined)) {
                this.localStorageService.set(this.commonConst.USER_ID, data.id);
            }

            //set user role
              this.localStorageService.set(this.commonConst.ROLE, data.role);

            return data.userName;
        });
    }

    public isAuthorize(): boolean {
        var authToken = this.localStorageService.get(this.commonConst.AUTH_TOKEN_KEY);
        return <boolean>(authToken || authToken != null);
    }

    public logoff() {
        this.localStorageService.remove(this.commonConst.AUTH_TOKEN_KEY);
        this.localStorageService.remove(this.commonConst.USER_ID);
        this.localStorageService.remove("debug");
        this.localStorageService.remove(this.commonConst.USER_ID);
    }

    public isAdmin() : boolean{
        var role = this.localStorageService.get(this.commonConst.ROLE);
        return <boolean>(role == "Admin");
    }
}