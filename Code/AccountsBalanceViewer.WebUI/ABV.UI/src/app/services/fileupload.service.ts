import { ICommonConst } from '../common/common';

export interface IFile {
    Id;
    FileDescription;
    Year;
    Month;
}

export class FileUploadService {

    /* @ngInject */
    constructor(
        private $http: ng.IHttpService,
        private commonConst: ICommonConst) {
    }

    getFiles(): ng.IPromise<IFile[]> {
        return this.$http.get(this.commonConst.API_HOST + 'api/File/GetAll').then(response => {
            return response.data;
        });
    }
    getReportData(year) {
        return this.$http.get(this.commonConst.API_HOST + 'api/GenerateReport?year=' + year).then(response => {
            return response.data;
        });
    }

    addFile(uploadedFile: any, File: IFile): ng.IPromise<IFile> {
        var config = <ng.IRequestShortcutConfig>{
            headers: { 'Content-Type': undefined },
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append('fileDetails', angular.toJson(data.File));
                for (var i = 0; i < data.files.length; i++) {
                    formData.append('files', uploadedFile[i]);
                }
                return formData;
            },
            data: { File: File, files: uploadedFile}
        };
        return this.$http.post(this.commonConst.API_HOST + 'api/File/AddFile', { File: File, files: uploadedFile }, config).then(response => {
            return response.data;
        });
    }
}