import { ICommonConst } from '../common/common';

export class AuthInterceptorService implements ng.IHttpInterceptor{

    // @ngInject
  static factory(
        $location: ng.ILocationService,
        $q: ng.IQService, 
        commonConst: ICommonConst, 
        localStorageService: angular.local.storage.ILocalStorageService): AuthInterceptorService {
        return new AuthInterceptorService($location, $q, commonConst, localStorageService);
    } 

   constructor(
        private $location: ng.ILocationService,
        private $q: ng.IQService, 
        private commonConst: ICommonConst, 
        private storageService: angular.local.storage.ILocalStorageService){
    }

    public request = (config) => {
        config.headers = config.headers || {};
        var authToken = this.storageService.get(this.commonConst.AUTH_TOKEN_KEY);
        if(authToken){
            config.headers.Authorization = 'Bearer ' + authToken;
        }
        return config;
    }

    public responseError = (rejection) => {
        if(rejection.status === 401){
            this.$location.path('/login')
        }
        return this.$q.reject(rejection);
    }
}