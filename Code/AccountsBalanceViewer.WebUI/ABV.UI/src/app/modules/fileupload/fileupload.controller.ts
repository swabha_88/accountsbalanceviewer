import { WebDevTecService, ITecThing } from '../components/webDevTec/webDevTec.service';
import { FileUploadService, IFile } from '../../services/fileupload.service';

export class FileUploadController {
    public file: IFile;
    public fileUploadForm: any;
    public commonErrorText: String;
    // public sourceLookups: ISource[];
      public months = [];
  public years = [];
    public uploader: any = new this.FileUploader();
      public toastr: any;

  /* @ngInject */
  constructor ($timeout: angular.ITimeoutService, webDevTec: WebDevTecService, toastr: any,
        private FileUploader: any, private fileUploadService : FileUploadService) {
    this.activate();
  }

  /** @ngInject */
  activate() {
      this.getMonths();
    this.getYears();
  }
 
   getMonths() {
    this.months = [ "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "Octomber",
            "November",
            "December"];
    return this.months;
  }

  getYears() {
    this.years = [2016, 2017,2018,2019,2020];
    return this.years;
  }
    uploadFile() {
        if (this.fileUploadForm.$valid) {
            var uploadedModule = this.uploader.queue.map(function (elem) {
                return elem._file;
            });
            this.fileUploadService.addFile(uploadedModule, this.file).then(response => {
                this.toastr.success('File updated successfully', 'Success!');
                this.$location.path('/dashboard');
            }).catch(error => {
                this.toastr.error('Sorry an error occured, File updating is failed', 'Oops Error!');
                console.log('Error Response: ', error);
                if (error.status == 400)
                    this.commonErrorText = error.data;
            })
        }
    }
}
