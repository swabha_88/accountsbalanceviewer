import { WebDevTecService, ITecThing } from '../components/webDevTec/webDevTec.service';
import { DashboardService, IAccounts } from '../../services/dashboard.service';
import { LookupService, ILookupConstant } from '../../services/lookup.service.ts';

export class DashboardController {
  public awesomeThings: ITecThing[];
  public webDevTec: WebDevTecService;
  public classAnimation: string;
  public creationDate: number;
  public toastr: any;
  public months = [];
  public years = [];
  public accountDetails = [];
  public filterAccountDetails = [];
  public month = "January";
  public year = 2016;

  /* @ngInject */
  constructor($timeout: angular.ITimeoutService, webDevTec: WebDevTecService, toastr: any,
  private dashboardService : DashboardService,
   private lookupService: LookupService,) {
    this.awesomeThings = new Array();
    this.webDevTec = webDevTec;
    this.classAnimation = '';
    this.creationDate = 1489822017468;
    this.toastr = toastr;
    this.activate($timeout);
  }

  /** @ngInject */
  activate($timeout: angular.ITimeoutService) {
    this.getWebDevTec();
    this.getLookups();
    //this.getYears();
    this.getaccountDetails();
    $timeout(() => {
      this.classAnimation = 'rubberBand';
    }, 4000);
  }

  showToastr() {
    this.toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
    this.classAnimation = '';
  }

  getWebDevTec() {
    this.awesomeThings = this.webDevTec.tec;
  }

  getLookups() {
     this.lookupService.getConstants().then(response => {
                    this.months = response.Months;
                    this.years = response.Years;
                });
  }

  getaccountDetails() {
    this.dashboardService.getExpenses(this.year, this.month).then(response => {
      console.log(response);
      this.filterAccountDetails = response;
            }).catch(error => {
                this.toastr.error('Sorry an error occured.', 'Oops Error!');
                console.log('Error Response: ', error);
                if (error.status == 400)
                    this.commonErrorText = error.data;
            })
  }
}
