import { AuthService } from '../../services/auth.service';

export interface IUser {
    userName: string;
    password: string;
}

export class LoginController {
    public user: IUser;
    public errorText: string;
    public loginForm: any;

    /* @ngInject */
    constructor(
        private $location: ng.ILocationService,
        private authService: AuthService,
        private toastr: any) {
    }

    public tryLogin() {
        if (this.loginForm.$valid) {
            this.authService.login(this.user.userName, this.user.password).then(response => {
                this.$location.path('/dashboard');
                this.toastr.info('Hi ' + response + ', Welcome');
            }).catch(error => {             
                this.errorText = (error.data != undefined)?error.data.error_description:error.message;
                this.toastr.error(this.errorText.toString(), 'Unauthorized Access!');
            });
        }
    }

}