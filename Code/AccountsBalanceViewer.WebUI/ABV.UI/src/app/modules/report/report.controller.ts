//import { FileUploadService } from '../../services/fileupload.service';
import { FileUploadService } from '../../services/fileupload.service';
import { LookupService, ILookupConstant } from '../../services/lookup.service.ts';

export class ReportController {

  public months = [];
  public Rnd = [];
  public Canteen = [];
  public CeoCarExpenses = [];
  public Marketing = [];
  public ParkingFines = [];
  public years =[];
  public year = 2017;

  public lineChartConfig = {
    options: {
      chart: {
        type: 'line'
      }
    },
    yAxis: {
      title: {
        text: 'Account Balance (Rs)'
      }
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },
    xAxis: {
      categories: this.months,

    },
    series: [{
      name: 'R&D',
      data: this.Rnd
    }, {
      name: 'Canteen',
      data: this.Canteen
    }, {
      name: 'CEOs Car',
      data: this.CeoCarExpenses
    }, {
      name: 'Marketing',
      data: this.Marketing
    }, {
      name: 'Parking Fine',
      data: this.ParkingFines
    }],
    title: {
      text: 'Account Balance from Jan to August'
    }
  }


  /* @ngInject */
  constructor(
    private fileUploadService: FileUploadService,
    private toastr: any,private lookupService: LookupService) {
     this.getLookups();
    this.activate()
  }

  activate() {
    this.getReportData(this.year);
  }


  getLookups() {
     this.lookupService.getConstants().then(response => {
                    this.months = response.Months;
                    this.years = response.Years;
                });
  }

  public getReportData(year) {

    this.fileUploadService.getReportData(year).then(data => {

      this.Canteen = data.Canteen;
      this.Rnd = data.RnD;
      this.Marketing = data.Marketing;
      this.CeoCarExpenses = data.CeoCar
      this.ParkingFines = data.ParkingFines;
      this.months = data.Months;
      this.generateReport();

    })
  }


  generateReport() {

    this.lineChartConfig = {
      options: {
        chart: {
          type: 'line'
        }
      },
      yAxis: {
        title: {
          text: 'Account Balance' + this.year + ' (Rs)'
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },
      xAxis: {
        categories: this.months,

      },
      series: [{
        name: 'R&D',
        data: this.Rnd
      }, {
        name: 'Canteen',
        data: this.Canteen
      }, {
        name: 'CEOs Car',
        data: this.CeoCarExpenses
      }, {
        name: 'Marketing',
        data: this.Marketing
      }, {
        name: 'Parking Fine',
        data: this.ParkingFines
      }],
      title: {
        text: 'Account Balance from Jan to August'
      }
    }

  }

}