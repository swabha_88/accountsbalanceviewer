import { AuthInterceptorService } from './services/authInterceptor.service';

/** @ngInject */
export function config($logProvider: angular.ILogProvider, toastrConfig: any,
 $httpProvider: angular.IHttpProvider,  $logProvider: angular.ILogProvider
) {
   
  
  // enable log
  $logProvider.debugEnabled(true);
  // set options third-party lib
  toastrConfig.allowHtml = true;
  toastrConfig.timeOut = 3000;
  toastrConfig.positionClass = 'toast-top-right';
  toastrConfig.preventDuplicates = true;
  toastrConfig.progressBar = true;

  
  $httpProvider.defaults.useXDomain = true;
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
  $httpProvider.defaults.withCredentials = false;
  $httpProvider.interceptors.push(AuthInterceptorService.factory)

}
