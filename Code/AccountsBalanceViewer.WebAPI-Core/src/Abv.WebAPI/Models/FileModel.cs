﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Abv.WebAPI.Models
{
    public class FileModel
    {
        [Column("1")]
        public string Header { get; set; }
        [Column("2")]
        public decimal Price { get; set; }
    }
}
