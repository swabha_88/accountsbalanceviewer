﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abv.Core.Repository;
using Abv.Dal;
using Abv.Dal.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity;

namespace Abv.WebAPI
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddDbContext<AbvContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DbConnection")));

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<AbvContext>()
                .AddDefaultTokenProviders();

            services.AddMvc();

            // Add application services.
            services.AddTransient<UnitOfWork, UnitOfWork>();
            services.AddTransient<ExpensesService, ExpensesService>();

            services.AddIdentity<IdentityUser, IdentityRole>(o =>
            {
                // configure identity options
                o.Password.RequireDigit = false;
                o.Password.RequireLowercase = false;
                o.Password.RequireUppercase = false;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequiredLength = 7;
            })
            .AddEntityFrameworkStores<AbvContext>()
            .AddDefaultTokenProviders();
            
            //services.AddDbContext<AbvWebContext>(options =>
            //        options.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=AbvWebContext-76dc45ed-f870-474d-999d-51881f7227d8;Trusted_Connection=True;MultipleActiveResultSets=true"));


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public async void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, 
            IServiceProvider serviceProvider, AbvContext dbcontext)
        {
              
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            //Set CORS headers
            app.Use(async (context, next) =>
            {
               // var appHost = Configuration.GetSection(ConfigKeys.AppHost);
               //// if (appHost.EndsWith("/")) { appHost = appHost.TrimEnd('/'); }

               // context.Response.Headers.Add("Access-Control-Allow-Origin", new[] { appHost.Value });
               // context.Response.Headers.Add("Access-Control-Allow-Headers", new[] { "Content-Type, x-xsrf-token" });
               // context.Response.Headers.Add("Access-Control-Allow-Methods", new[] { "GET, POST, PUT, DELETE, OPTIONS" });
               // context.Response.Headers.Add("Access-Control-Allow-Credentials", "true");

               await next();
            });


            app.UseIdentity();

            app.UseMvc(routes =>
            {
                routes.MapRoute("areaRoute", "{area:exists}/{controller=Admin}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            await CreateRoles(dbcontext, serviceProvider);

        }

        private async Task CreateRoles(AbvContext context, IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>();
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();

            List<IdentityRole> roles = new List<IdentityRole>();
            roles.Add(new IdentityRole { Name = "admin", NormalizedName = "ADMIN" });
            roles.Add(new IdentityRole { Name = "user", NormalizedName = "USER" });

            foreach (var role in roles)
            {
                var roleExit = await roleManager.RoleExistsAsync(role.Name);
                if (!roleExit)
                {
                    context.Roles.Add(role);
                    context.SaveChanges();
                }
            }

            await CreateUser(context, userManager);
        }

        private async Task CreateUser(AbvContext context, UserManager<IdentityUser> userManager)
        {
            const string AdminUserName = "admin";
            const string AdminPassword = "password";

            const string UserUserName = "user";
            const string UserPassword = "password";

            var adminUser = await userManager.FindByNameAsync(AdminUserName);
            var userUser = await userManager.FindByNameAsync(UserUserName);

            if (adminUser == null)
            {
                var user = new IdentityUser()
                {
                    UserName = AdminUserName,
                };

                var result = await userManager.CreateAsync(user, AdminPassword);

                if (!result.Succeeded)
                {
                    throw new Exception(string.Format("Admin User couldn't be created. {0}", string.Join(",", result)));
                }

                await userManager.AddToRoleAsync(user, "admin");
            }


            if (userUser == null)
            {
                var user = new IdentityUser()
                {
                    UserName = UserUserName,
                };

                var result = await userManager.CreateAsync(user, UserPassword);

                if (!result.Succeeded)
                {
                    throw new Exception(string.Format("User couldn't be created. {0}", string.Join(",", result)));
                }

                await userManager.AddToRoleAsync(user, "user");
            }
        }
    }
}
