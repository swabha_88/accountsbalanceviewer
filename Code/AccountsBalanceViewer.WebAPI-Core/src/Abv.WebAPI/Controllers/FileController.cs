using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Abv.Core.Repository;
using Abv.Dal;
using Abv.Dal.Entity;
using Abv.Dal.Services;
using Abv.WebAPI.Models;
using CsvHelper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;

namespace Abv.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/File")]
    public class FileController : Controller
    {
        private readonly ExpensesService _expensesService;
        public FileController(ExpensesService expensesService)
        {

            _expensesService = expensesService;
        }

        [HttpGet]
        public IEnumerable<Expenses> GetAll()
        {
            return _expensesService.GetAllExpensess();
        }

        [HttpPost]
        public IActionResult AddFile(IFormFile file)
        {
            try
            {
                if (file == null)
                    return Json(new { success = false });

                if (file.Length <= 5412454)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');// FileName returns "fileName.ext"(with double quotes) in beta 3

                    //var storedFileName = _imageHelper.SaveFile(fileName, file);

                    return Json(new
                    {
                        success = true,
                        fileName = fileName,
                        //storedFileName = storedFileName
                    });
                }

                return Json(new
                {
                    success = false,
                });
            }
            catch (Exception ex)
            {
                //_log.Error("Error : " + ex.Message + "StackTrace : " + ex.StackTrace);
                throw;
            }

        }
        //[HttpPost]
        //[Route("AddFile")]
        //public string AddFile()
        //{
        //    try
        //    {

        //        string fileName = @"D:\Doc\Collection.xlsx";
        //       // var package = new ExcelPackage(new FileInfo("sample.xlsx"));
        //        // var products = new ExcelMapper("products.xlsx").Fetch<FileModel>();
        //        return "OK";
        //    }
        //    catch (Exception e)
        //    {
        //        return "OK";
        //        //return StatusCode(500);
        //    }
        //    // string fileName = @"D:\Doc\Collection.xlsx";
        //    //var csv = new CsvReader(new StringReader(fileName));

        //    // while (csv.Read())
        //    // {
        //    //     // var record = csv.GetRecords();
        //    //     var record = csv.GetRecords<FileModel>();
        //    // }

        //}
    }
}