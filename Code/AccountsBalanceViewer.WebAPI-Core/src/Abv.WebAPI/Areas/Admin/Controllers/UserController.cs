using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace Abv.Web.Controllers
{
    //public class UserController : Controller
    //{
    //    private readonly UserService _userService;
    //    private readonly SecurityService _securityService;
    //    private readonly UserManager<AbvUser> _userManager;
    //    private readonly SignInManager<AbvUser> _signInManager;

    //    public UserController(UserService userService, SecurityService securityService, UserManager<AbvUser> userManager, SignInManager<AbvUser> signInManager)
    //    {
    //        _userService = userService;
    //        _securityService = securityService;
    //        _userManager = userManager;
    //        _signInManager = signInManager;
    //    }
    //    // GET: /User/
    //    public IActionResult Index()
    //    {
    //        var users = _userService.GetAllUsers().ToList().Select(c => c.ToUserViewModel());
    //        return View(users);
    //    }
    //    //GET : /User/Create
    //    public IActionResult Create()
    //    {
    //        SelectList status = new SelectList(Constants.UsState);
    //        ViewBag.Status = status;
    //        return View();
    //    }

    //    //POST : /User/Create
    //    [HttpPost]
    //    public async Task<IActionResult> Create(UserViewModel user)
    //    {
    //        if (ModelState.IsValid)
    //        {
    //            try
    //            {
    //                var result = await _securityService.Register(new RegisterRequest()
    //                {
    //                    Email = user.Email,
    //                    Password = user.Password,
    //                    UserType = user.UserType
    //                });

    //                if (result.Succeeded)
    //                {
    //                    var identityUser = await _userManager.FindByNameAsync(user.Email);

    //                    var model = new SystemUser
    //                    {
    //                        FirstName = user.FirstName,
    //                        LastName = user.LastName,
    //                        Address1 = user.Address1,
    //                        Address2 = user.Address2,
    //                        City = user.City,
    //                        State = user.State,
    //                        Email = user.Email,
    //                        Identity = identityUser.Id,
    //                        UserType = user.UserType
    //                    };

    //                    _userService.AddUser(model);
    //                    return RedirectToAction("Index");
    //                }
    //            }
    //            catch (ServiceException e)
    //            {
    //                ModelState.AddModelError(string.Empty, e.Message);
    //                return View(user);
    //            }

    //        }
    //        return View(user);
    //    }

    //    //// GET: User/Edit/5
    //    public IActionResult Edit()
    //    {

    //        var identity = _userManager.GetUserId(User);

    //        if (identity == null)
    //        {
    //            return NotFound();
    //        }

    //        var user = _userService.Get(x => x.Identity == identity).FirstOrDefault();

    //        if (user == null)
    //        {
    //            return NotFound();
    //        }

    //        SelectList status = new SelectList(Constants.UsState);
    //        ViewBag.Status = status;
    //        return View(user);
    //    }

    //    // POST: User/Edit/5
    //    [HttpPost]
    //    public async Task<IActionResult> Edit(SystemUser updatedUser)
    //    {
    //        var user = _userService.GetUserById(updatedUser.Id);
    //        updatedUser.Identity = user.Identity;

    //        if (user.UserType != updatedUser.UserType)
    //        {
    //            updatedUser.Identity = user.Identity;
    //            await _securityService.UpdateRole(updatedUser);
    //        }

    //        if (ModelState.IsValid)
    //        {
    //            try
    //            {
    //                _userService.UpdateUser(updatedUser);
    //            }
    //            catch (DbUpdateConcurrencyException)
    //            {
    //                if (!UserExists(updatedUser.Id))
    //                {
    //                    return NotFound();
    //                }
    //                else
    //                {
    //                    throw;
    //                }
    //            }
    //            return RedirectToAction("Edit");
    //        }
    //        return View(updatedUser);
    //    }


    //    //
    //    // GET: /Manage/ChangePassword
    //    public ActionResult ChangePassword(int id)
    //    {
    //        ViewBag.UserId = id;
    //        return View();
    //    }

    //    //
    //    // POST: /Manage/ChangePassword
    //    [HttpPost]
    //    public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
    //    {
    //        if (!ModelState.IsValid)
    //        {
    //            return View(model);
    //        }
    //        try
    //        {
    //            var user = _userService.GetUserById(model.Id);
    //            var requestModel = new ChangePasswordRequest
    //            {
    //                Identity = user.Identity,
    //                Email = User.Identity.Name,
    //                OldPassword = model.OldPassword,
    //                NewPassword = model.NewPassword
    //            };
    //            var result = await _securityService.ChangePassword(requestModel);

    //            if (!result.Succeeded)
    //            {
    //                return View(model);
    //            }
    //        }
    //        catch (ServiceException e)
    //        {
    //            ModelState.AddModelError(string.Empty, e.Message);
    //            return View(model);
    //        }

    //        return RedirectToAction("Edit", new { model.Id });
    //    }


    //    #region private methods

    //    private bool UserExists(long id)
    //    {
    //        var user = _userService.GetUserById(id);

    //        if (user != null)
    //            return true;

    //        return false;
    //    }

    //}
    //#endregion

}