﻿$(document).ready(function () {

    //Initialize multiselect
    $('.multi-select').select2({
        tags: true
    });

    //initialize datepicker
    $(".datepicker").datepicker(
    {
        dateFormat: "yy-mm-dd"
    }
        );

    ////To disable mailing address feild in Customer create section
    //function disableMailingAddress($event) {
    //    debugger 
    //}

    toastr.options = {
        "debug": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "fadeIn": 300,
        "fadeOut": 1000,
        "timeOut": 1000,
        "extendedTimeOut": 1000
    }

    $('#CustomerId').change(function (attr) {
        $.ajax({
            url: '/Device/GetBarcodes/' + $('#CustomerId').val(),
            type: 'get',
            data: $(this).serialize(),
            success: function (response) {
                var mySelect = $('#BarcodeId');
                mySelect.empty();
                $.each(response, function (val, text) {
                    mySelect.append(
                        $('<option></option>').val(text.Id).html(text.BarcodeValue)
                    );
                });

            }
        });
    });

});

