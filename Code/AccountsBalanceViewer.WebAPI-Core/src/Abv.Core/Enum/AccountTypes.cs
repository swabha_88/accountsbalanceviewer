﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Abv.Core.Enum
{
    public enum AccountTypes
    {
        [Display(Name = "R&D")]
        RAndD,
        Canteen,
        [Display(Name = "CEO’s car expenses")]
        CeoCarExpenses,
        Marketing,
        [Display(Name = "Parking fines")]
        ParkingFines
    }
}
