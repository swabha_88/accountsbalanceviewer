﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abv.Core.Exception
{
    public class ServiceException : System.Exception
    {
        public ServiceException(string message) : base(message)
        {

        }

        public ServiceException(string message, System.Exception innerException) : base(message, innerException)
        {

        }
    }
}
