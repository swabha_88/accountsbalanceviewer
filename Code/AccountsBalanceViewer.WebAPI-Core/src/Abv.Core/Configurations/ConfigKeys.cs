﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abv.Core.Configurations
{
    public class ConfigKeys
    {
        public const string ApiHost = "Data:ApiHost";
        public const string AppHost = "Data:AppHost";
    }
}
