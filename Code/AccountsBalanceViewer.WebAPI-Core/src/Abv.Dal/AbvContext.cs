﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abv.Dal.Entity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Abv.Dal
{
    public class AbvContext : IdentityDbContext<IdentityUser>
    {
        public AbvContext(DbContextOptions<AbvContext> options) : base(options)
        {
        }

        public DbSet<Expenses> Expenses { get; set; }
    }
}
