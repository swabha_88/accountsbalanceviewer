﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abv.Dal.Interfaces
{
    public interface IGenericRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        //IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        void Add(T entity);
        T GetByID(object id);
        void Delete(T entity);
        void Edit(T entity);
        void Save();
    }
}
