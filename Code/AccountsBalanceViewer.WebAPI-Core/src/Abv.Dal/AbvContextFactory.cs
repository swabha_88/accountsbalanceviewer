﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Abv.Dal
{
    /// <summary>
    /// No parameterless constructor was found on 'AbvContext'. Either add a parameterless constructor to 'AbvContext' or add an implementation of 'IDbContextFactory<AvContext>' in the same assembly as 'AbvContext'.
    /// </summary>

    public class AbvContextFactory : IDbContextFactory<AbvContext>
    {
        public AbvContext Create()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AbvContext>();
            optionsBuilder.UseSqlServer("Data Source=.\\SQLEXPRESS;Initial Catalog=abv-db;Integrated Security=True;MultipleActiveResultSets=True;");
            return new AbvContext(optionsBuilder.Options);
        }
    }
}
