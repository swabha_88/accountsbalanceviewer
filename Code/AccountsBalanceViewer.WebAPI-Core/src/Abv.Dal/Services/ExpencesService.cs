﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Abv.Core.Exception;
using Abv.Core.Repository;
using Abv.Dal.Entity;
using Microsoft.EntityFrameworkCore;

namespace Abv.Dal.Services
{
    public class ExpensesService
    {
        private readonly UnitOfWork _unitOfWork;
        public ExpensesService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IQueryable<Expenses> GetAllExpensess()
        {
            return _unitOfWork.ExpensesRepository.GetAll();
        }


        public IQueryable<Expenses> Get(Expression<Func<Expenses, bool>> filter = null, params Expression<Func<Expenses, object>>[] includeProperties)
        {
            return _unitOfWork.ExpensesRepository.Get(filter, includeProperties);
        }

        public Expenses AddExpenses(Expenses entity)
        {
            _unitOfWork.ExpensesRepository.Add(entity);
            _unitOfWork.Save();
            return entity;
        }

        public void UpdateExpenses(Expenses entity)
        {
            _unitOfWork.ExpensesRepository.Edit(entity);
            _unitOfWork.Save();
        }

        public Expenses GetExpensesById(int? id)
        {
            return _unitOfWork.ExpensesRepository.GetByID(id);
        }
        public void Delete(Expenses entity)
        {
            try
            {
                _unitOfWork.ExpensesRepository.Delete(entity);
                _unitOfWork.Save();
            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;
                if (sqlException != null && sqlException.Number == 547)
                    throw new ServiceException("Operation cannot be completed because this Expenses is in use.", ex);
                throw ex;
            }
        }

    }
}
