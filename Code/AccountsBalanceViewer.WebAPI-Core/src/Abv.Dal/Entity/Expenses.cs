﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abv.Core.Enum;
using Abv.Dal.Base;

namespace Abv.Dal.Entity
{
    public class Expenses : BaseEntity
    {
        public AccountTypes AccountType { get; set; }
        public decimal Price { get; set; }
        public string Month { get; set; }
        public int Year { get; set; }

        //Created User
        public string Identity { get; set; }
    }
}
