﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Abv.Dal.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Abv.Dal.Repository
{
    public class GenericRepository<T> :
     IGenericRepository<T> where T : class
    {
        private readonly AbvContext _context;
        internal DbSet<T> dbSet;

        public GenericRepository(AbvContext context)
        {
            _context = context;
            dbSet = _context.Set<T>();
        }

        public virtual IQueryable<T> GetAll()
        {

            IQueryable<T> query = _context.Set<T>();
            return query;
        }

        public virtual T GetByID(object id)
        {
            return Find(_context.Set<T>(), id);
        }

        public virtual void Add(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public virtual void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public virtual void Edit(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Save()
        {
            _context.SaveChanges();
        }

        public virtual IQueryable<T> Get(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includeProperties)
        {
            var query = GetQuery(filter, includeProperties);
            return query;
        }


        #region Private Methods

        private IQueryable<T> GetQuery(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = dbSet;

            if (includeProperties != null)
                foreach (var includeProperty in includeProperties)
                {
                    query = query.Include(includeProperty);
                }

            //Type[] types = typeof(T).GetInterfaces();

            //if (types.Contains(typeof(IDeletable)))
            //{
            //    query = ((IQueryable<IDeletable>)query).Where(c => !c.IsDeleted).Cast<T>();
            //}

            if (filter != null)
            {
                query = query.Where(filter);
            }

            return query;
        }



        private T Find<T>(DbSet<T> set, params object[] keyValues)
            where T : class
        {
            var entityType = _context.Model.FindEntityType((typeof(T)));
            var key = entityType.FindPrimaryKey();

            var entries = _context.ChangeTracker.Entries<T>();

            var i = 0;
            foreach (var property in key.Properties)
            {
                var keyValue = keyValues[i];
                entries = entries.Where(e => e.Property(property.Name).CurrentValue == keyValue);
                i++;
            }

            var entry = entries.FirstOrDefault();
            if (entry != null)
            {
                // Return the local object if it exists.
                return entry.Entity;
            }

            // TODO: Build the real LINQ Expression
            // set.Where(x => x.Id == keyValues[0]);
            var parameter = Expression.Parameter(typeof(T), "x");
            var query = set.Where((Expression<Func<T, bool>>)
                Expression.Lambda(
                    Expression.Equal(
                        Expression.Property(parameter, "Id"),
                        Expression.Constant(keyValues[0])),
                    parameter));

            // Look in the database
            return query.AsNoTracking().FirstOrDefault();
        }

        #endregion
    }
}
