﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abv.Dal;
using Abv.Dal.Entity;
using Abv.Dal.Repository;

namespace Abv.Core.Repository
{
    public class UnitOfWork
    {
        private readonly AbvContext _context;

        private GenericRepository<Expenses> _expensesRepository;

        public UnitOfWork(AbvContext context)
        {
            _context = context;
        }

        public GenericRepository<Expenses> ExpensesRepository => _expensesRepository ?? (_expensesRepository = new GenericRepository<Expenses>(_context));
     
        public void Save()
        {
            _context.SaveChanges();
        }

    }
}
