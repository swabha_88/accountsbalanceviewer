﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abv.Dal.Entity.BaseEntity
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            this.CreatedDate = DateTimeOffset.UtcNow;
            this.UpdatedDate = DateTimeOffset.UtcNow;
        }

        [Key]
        public int Id { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset UpdatedDate { get; set; }
    }
}
