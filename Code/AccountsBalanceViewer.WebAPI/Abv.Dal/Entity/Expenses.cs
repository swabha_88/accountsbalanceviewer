﻿using Abv.Core.Enums;
using Abv.Dal.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abv.Dal.Entity
{
    public class Expenses : BaseEntity.BaseEntity
    {
        public AccountTypes AccountType { get; set; }
        public decimal Price { get; set; }
        public string Month { get; set; }
        public int Year { get; set; }

        //Created User
        public string Identity { get; set; }
    }
}
