﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abv.Dal.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Abv.Dal
{
    public class AbvContext : IdentityDbContext<AbvUser>
    {
        public AbvContext()
            : base("ABVConnection")
        {
            //this.Configuration.ProxyCreationEnabled = false;
        }
        public DbSet<Expenses> Expenses { get; set; }
    }
}
