using Abv.Core.Constants;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Abv.Dal.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Abv.Dal.AbvContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }


        protected override void Seed(Abv.Dal.AbvContext context)
        {
            // To Add Roles
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            if (!roleManager.RoleExists(RoleNames.User))
                roleManager.Create(new IdentityRole(RoleNames.User));

            if (!roleManager.RoleExists(RoleNames.Admin))
                roleManager.Create(new IdentityRole(RoleNames.Admin));

            AddUsers(context);
        }

        private void AddUsers(AbvContext context)
        {

            // To Add admin
            var admin = new AbvUser()
            {
                UserName = "admin",
                Email = "admin@testmail.com",
            };

            var userManager = new UserManager<AbvUser>(new UserStore<AbvUser>(context));

            if (userManager.FindByName("admin") == null)
            {
                userManager.Create(admin, "1234qwer");
                userManager.AddToRole(admin.Id, RoleNames.Admin);
            }

            // To Add user
            var user = new AbvUser()
            {
                UserName = "user",
                Email = "user@testmail.com",
            };

            if (userManager.FindByName("user") == null)
            {
                userManager.Create(user, "1234qwer");
                userManager.AddToRole(user.Id, RoleNames.User);
            }


        }
    }
}
