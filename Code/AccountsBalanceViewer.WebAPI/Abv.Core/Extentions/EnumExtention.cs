﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abv.Core.Extentions
{
    public static class EnumExtention
    {
        public static int GetValue(this string enumobject)
        {
            var value = enumobject.Trim().ToLower();

            switch (value)
            {
                case "r&d":    
                        return 1;
                case "canteen":
                    return 2;
                case "ceo’s car expenses":
                    return 3;
                case "marketing":
                    return 4;
                case "parking fines":
                    return 5;
                default:
                    return 0;
            }
        }
    }
}
