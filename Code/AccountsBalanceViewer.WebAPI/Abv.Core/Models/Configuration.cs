﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abv.Core.Models
{
    public class Configuration
    {
        public string ApiHost { get; set; }
        public string AppHost { get; set; }
    }
}
