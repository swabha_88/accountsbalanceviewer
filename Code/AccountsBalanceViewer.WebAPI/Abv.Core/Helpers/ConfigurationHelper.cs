﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Abv.Core.Models;

namespace Abv.Core.Helpers
{
    public static class ConfigurationHelper
    {

        private static Configuration _configuration;

        public static Configuration Configuration
        {
            get { return _configuration ?? (_configuration = GetConfiguration()); }
        }

        private static Configuration GetConfiguration()
        {
            var appSettings = WebConfigurationManager.AppSettings;

            return new Configuration()
            {
                AppHost = appSettings["appHost"],
                ApiHost = appSettings["apiHost"],
            };
        }

    }
}
