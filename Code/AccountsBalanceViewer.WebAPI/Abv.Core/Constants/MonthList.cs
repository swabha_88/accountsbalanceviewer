﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abv.Core.Constants
{
    public partial class Constants
    {
        public static List<string> YearList = new List<string>()
        {
              "2013",
            "2014",
            "2015",
            "2016",
            "2017",
            "2018",
            "2019",
            "2020"
        };
    }
}
