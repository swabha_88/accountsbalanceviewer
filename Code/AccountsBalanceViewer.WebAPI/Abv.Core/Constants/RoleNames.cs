﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abv.Core.Constants
{
    public class RoleNames
    {
        /// Administrators : Admins can be upload expenses sheets and view reports.
        /// </summary>
        public const string Admin = "Admin";
        /// <summary>
        /// Users : Only can view the details.
        /// </summary>
        public const string User = "User";
    }
}
