﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abv.Core.Enums
{
    public enum AccountTypes
    {
        None,
        [Display(Name = "R&D")]
        RAndD,
        Canteen,
        [Display(Name = "CEO’s car expenses")]
        CeoCarExpenses,
        Marketing,
        [Display(Name = "Parking fines")]
        ParkingFines
    }
}
