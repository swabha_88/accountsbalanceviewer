﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abv.Dal;
using Abv.Dal.Entity;

namespace Abv.Infrastructure.Repository
{
    public class UnitOfWork
    {
        private readonly AbvContext _context;
        private bool _disposed = false;

        private GenericRepository<Expenses> _expensesRepository;

        public UnitOfWork(AbvContext context)
        {
            _context = context;
        }

        public GenericRepository<Expenses> ExpensesRepository => _expensesRepository ?? (_expensesRepository = new GenericRepository<Expenses>(_context));

        public void Save()
        {
            _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
