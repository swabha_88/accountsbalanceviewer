﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Abv.Dal.Entity;
using Abv.Infrastructure.Repository;

namespace Abv.Infrastructure.Services
{
    public class ExpensesService : IDisposable
    {
        private readonly UnitOfWork _unitOfWork;

        public ExpensesService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Expenses> GetExpensess(int year,string month)
        {
            return _unitOfWork.ExpensesRepository.Get(x=> x.Year == year && x.Month.ToLower() == month);
        }

        public IEnumerable<Expenses> GetExpensesByYear(int year)
        {
            return _unitOfWork.ExpensesRepository.Get(x => x.Year == year);
        }

        public Expenses GetExpenses(int id)
        {
            return _unitOfWork.ExpensesRepository.GetByID(id);
        }

        public void UpdateExpenses(Expenses entity)
        {
            var existingExpenses = _unitOfWork.ExpensesRepository.GetByID(entity.Id);

            if (existingExpenses == null)
                throw new Exception("Expenses does not exist. Invalid entity");

            _unitOfWork.ExpensesRepository.Update(existingExpenses);
            _unitOfWork.Save();
        }

        public void AddExpenses(Expenses entity)
        {
            _unitOfWork.ExpensesRepository.Insert(entity);
            _unitOfWork.Save();
        }


        public void DeleteExpenses(Expenses entity)
        {
            _unitOfWork.ExpensesRepository.Delete(entity);
            _unitOfWork.Save();
        }

        public void Dispose()
        {
            if (_unitOfWork != null)
                _unitOfWork.Dispose();
        }
    }
}
