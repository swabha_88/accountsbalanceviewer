﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ganss.Excel;

namespace Abv.WebAPI.Models
{
    public class CsvHeader
    {
        [Column(1)]
        public string AccountType { get; set; }
        [Column(2)]
        public int Price { get; set; }
    }
}