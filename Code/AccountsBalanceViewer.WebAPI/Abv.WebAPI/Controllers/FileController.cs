﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Abv.Core.Enums;
using Abv.Dal.Entity;
using Abv.WebAPI.Models;
using Ganss.Excel;
using Newtonsoft.Json;
using WebGrease.Css.Extensions;
using Abv.Core.Extentions;
using Abv.Dal;
using Abv.Infrastructure.Repository;
using Abv.Infrastructure.Services;

namespace Abv.WebAPI.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/File")]
    public class FileController : ApiController
    {
        private readonly ExpensesService _expensesService;
        private readonly AbvContext _context;
        public FileController()
        {
            _context = new AbvContext();
            _expensesService = new ExpensesService(new UnitOfWork(_context));
        }

        [Authorize(Roles = "Admin")]
        [Route("api/File/AddFile")]
        [HttpPost]
        public async Task<HttpResponseMessage> AddFile()
        {
            string fileName = "";

            // Creating Multistream provider
            if (!Request.Content.IsMimeMultipartContent())
            {
                return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
            }

            var temporaryDownloadLocation = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["IncomingFilesDownloadDirectoryVirtualPath"]);

            bool exists = System.IO.Directory.Exists(temporaryDownloadLocation);
            if (!exists)
                System.IO.Directory.CreateDirectory(temporaryDownloadLocation);

            var multipartStreamProvider = new MultipartFormDataStreamProvider(temporaryDownloadLocation);

            try
            {
                // Save tempory file to tempory location
                await Request.Content.ReadAsMultipartAsync(multipartStreamProvider);

                foreach (var fileData in multipartStreamProvider.FileData)
                {
                    if (string.IsNullOrEmpty(fileData.Headers.ContentDisposition.FileName))
                    {
                        return Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted");
                    }
                    fileName = fileData.Headers.ContentDisposition.FileName;
                    if (fileName.StartsWith("\"") && fileName.EndsWith("\""))
                    {
                        fileName = fileName.Trim('"');
                    }
                    if (fileName.Contains(@"/") || fileName.Contains(@"\"))
                    {
                        fileName = Path.GetFileName(fileName);
                    }

                    // Check if file exist in orginal location & if so delete it
                    if (File.Exists(Path.Combine(temporaryDownloadLocation, fileName)))
                        File.Delete(Path.Combine(temporaryDownloadLocation, fileName));

                    // Save orginal file into location
                    var orginalFilePath = Path.Combine(temporaryDownloadLocation, fileName);
                    File.Move(fileData.LocalFileName, orginalFilePath);

                    var csvData = new ExcelMapper(orginalFilePath) { HeaderRow = false }.Fetch<CsvHeader>();

                    var fileDetails = multipartStreamProvider.FormData["fileDetails"];
                    if (fileDetails == null)
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);

                    }
                    var model = JsonConvert.DeserializeObject<CsvModel>(multipartStreamProvider.FormData["fileDetails"]);

                    var expenses = new List<Expenses>();

                    foreach (var data in csvData)
                    {
                        expenses.Add(new Expenses()
                        {
                            Year = model.Year,
                            Month = model.Month,
                            Identity = "32764832687634876",
                            AccountType = (AccountTypes)EnumExtention.GetValue(data.AccountType),
                            Price = data.Price
                        });
                    }

                    expenses.ForEach(x => { _expensesService.AddExpenses(x); });
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
            finally
            {
                // Delete Tempory File
                if (multipartStreamProvider.FileData != null)
                    foreach (var file in multipartStreamProvider.FileData) { File.Delete(file.LocalFileName); }
                // Delete orginal file
                if (File.Exists(Path.Combine(temporaryDownloadLocation, fileName)))
                    File.Delete(Path.Combine(temporaryDownloadLocation, fileName));
            }

            return Request.CreateResponse(HttpStatusCode.OK, "");
        }

    }
}
