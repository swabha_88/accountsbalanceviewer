﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Abv.Core.Constants;

namespace Abv.WebAPI.Controllers
{
    [Route("api/lookup")]
    public class LookupController : ApiController
    {
        [HttpGet]
        [Route("api/lookup/GetConstants")]
        public IHttpActionResult GetConstants()
        {
            var result = new
            {
                Years = Constants.YearList,
                Months = Constants.MonthList
            };

            return Json(result);
        }
    }
}
