﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Abv.Core.Enums;
using Abv.Dal;
using Abv.Infrastructure.Services;
using Abv.Dal.Entity;
using Abv.Infrastructure.Repository;
using Abv.WebAPI.Models;

namespace Abv.WebAPI.Controllers
{
    /// <summary>
    ///  ExpensesController for Expenses entity CRUD operartions 
    /// </summary>
    [Authorize]
    [Route("api/Expenses")]
    public class ExpensesController : ApiController
    {
        private readonly ExpensesService _expensesService;
        private readonly AbvContext _context;

        public ExpensesController()
        {
            _context = new AbvContext();
            _expensesService = new ExpensesService(new UnitOfWork(_context));
        }

        ///  GET api/Expenses 
        /// <returns>All Expensess as IEnumerable</returns>
        [HttpGet]
        [Route("api/Expenses")]
        [ResponseType(typeof(IEnumerable<Expenses>))]
        public IHttpActionResult GetExpensess(int year,string month)
        {
            var model = _expensesService.GetExpensess(year, month);
            return Ok(model);
        }

        [Route("api/GenerateReport")]
        [HttpGet]
        public IHttpActionResult GenerateReport(int year)
        {

            var expenses = _expensesService.GetExpensesByYear(year);

            var balanceDetails = (from ex in expenses
                                  group ex by ex.AccountType into g
                                  select new
                                  {
                                      Types = g.Key,
                                      Prices = g.Select(c => c.Price).ToList(),
                                      Months = g.Select(c => c.Month).ToList()
                                  }
                   ).ToList();


            var Canteen = balanceDetails.Where(v => v.Types == AccountTypes.Canteen).FirstOrDefault();
            var RnD = balanceDetails.Where(v => v.Types == AccountTypes.RAndD).FirstOrDefault();
            var Marketing = balanceDetails.Where(v => v.Types == AccountTypes.Marketing).FirstOrDefault();
            var CeoCar = balanceDetails.Where(v => v.Types == AccountTypes.CeoCarExpenses).FirstOrDefault();
            var ParkingFines = balanceDetails.Where(v => v.Types == AccountTypes.ParkingFines).FirstOrDefault();
            var Months = balanceDetails.FirstOrDefault();

            var report = new ReportModel()
            {
                Canteen = Canteen != null ? Canteen.Prices : new List<decimal>(),
                RnD = RnD != null ? RnD.Prices : new List<decimal>(),
                Marketing = Marketing != null ? Marketing.Prices : new List<decimal>(),
                CeoCar = CeoCar != null ? CeoCar.Prices : new List<decimal>(),
                ParkingFines = ParkingFines != null ? ParkingFines.Prices : new List<decimal>(),
                Months = Months != null ? Months.Months : new List<string>(),

            };
            return Ok(report);
        }

        // GET api/Expenses/5
        /// <returns>All Expenses selected by Id</returns>
        [HttpGet]
        [Route("api/Expenses/{id}")]
        [ResponseType(typeof(Expenses))]
        public IHttpActionResult GetExpenses(int id)
        {
            Expenses expenses = _expensesService.GetExpenses(id);
            if (expenses == null)
                return NotFound();

            return Ok(expenses);
        }

        // PUT api/Expenses/5
        /// <summary>Update existing Expenses</summary>
        [HttpPut]
        [Route("api/Expenses/{id}")]
        [ResponseType(typeof(Expenses))]
        public IHttpActionResult PutExpenses(int id, Expenses expenses)
        {
            if (id != expenses.Id)
                return BadRequest();

            _expensesService.UpdateExpenses(expenses);
            return Ok(expenses);
        }

        // POST api/Expenses
        /// <summary>Add new Expenses</summary>
        [HttpPost]
        [Route("api/Expenses")]
        [ResponseType(typeof(Expenses))]
        public IHttpActionResult PostExpenses(Expenses expenses)
        {
            _expensesService.AddExpenses(expenses);
            return Ok(expenses);
        }

        // DELETE api/Expenses/5
        /// <summary>Delete existing Expenses</summary>
        [HttpDelete]
        [Route("api/Expenses/{id}")]
        [ResponseType(typeof(Expenses))]
        public IHttpActionResult DeleteExpenses(int id)
        {
            Expenses expensesToDelete = _expensesService.GetExpenses(id);
            if (expensesToDelete == null)
                return NotFound();

            _expensesService.DeleteExpenses(expensesToDelete);
            return Ok(expensesToDelete);
        }

        // Disposing
        protected override void Dispose(bool disposing)
        {
            if (disposing && _expensesService != null)
            {
                _expensesService.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
