﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Abv.Core.Helpers;
using Abv.Dal;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Abv.WebAPI.Models;

namespace Abv.WebAPI.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;

        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }

        //public override Task MatchEndpoint(OAuthMatchEndpointContext context)
        //{
        //    var appHost = ConfigurationHelper.Configuration.AppHost;
        //    if (appHost.EndsWith("/"))
        //    {
        //        appHost = appHost.TrimEnd('/');
        //    }

        //    if (context.IsTokenEndpoint && context.Request.Method == "OPTIONS")
        //    {
        //        context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { appHost });
        //        context.OwinContext.Response.Headers.Append("Access-Control-Allow-Credentials", "true");

        //        //    context.OwinContext.Response.Headers.Add("Access-Control-Allow-Headers", new[] { "authorization" });
        //        context.RequestCompleted();
        //        return Task.FromResult(0);
        //    }

        //    return base.MatchEndpoint(context);
        //}
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var appHost = ConfigurationHelper.Configuration.AppHost;
            if (appHost.EndsWith("/"))
            {
                appHost = appHost.TrimEnd('/');
            }

            //var allowedOrigins = new[] { appHost };
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            context.OwinContext.Response.Headers.Append("Access-Control-Allow-Credentials", "true");
            //if (!context.OwinContext.Response.Headers.ContainsKey("Access-Control-Allow-Origin"))
            //{
            //    context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", allowedOrigins);
            //    context.OwinContext.Response.Headers.Add("Access-Control-Allow-Credentials", true);
            //}

            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

            AbvUser user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
               OAuthDefaults.AuthenticationType);
            ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager,
                CookieAuthenticationDefaults.AuthenticationType);

            var userRole = userManager.GetRoles(user.Id).FirstOrDefault();

            AuthenticationProperties properties = CreateProperties(user.UserName, userRole);
            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(cookiesIdentity);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        //public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        //{
        //    // Resource owner password credentials does not provide a client ID.
        //    //if (context.ClientId == null)
        //    //{
        //    //    context.Validated();
        //    //}

        //    //return Task.FromResult<object>(null);
        //    context.Validated();
        //}

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName, string role)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName },
                { "role", role }
            };
            return new AuthenticationProperties(data);
        }
    }
}